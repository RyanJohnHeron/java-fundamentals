public class Pet {

    private String name;
    public int numLegs;
    public static int numPets = 0;

    public void feed() {
        System.out.println("Feed generic pet some yummy food.\n");
    }


    // GETTERS & SETTERS //

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public static int getNumPets() { return numPets; }

    public static void setNumPets(int numPets) { Pet.numPets = numPets; }

}
