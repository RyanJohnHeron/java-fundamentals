public class EntryPoint {

    private static String nameOfProgram;

    public static void main(String[] args)
    {

        nameOfProgram = "First Program";

        Pet myPet = new Pet();

        myPet.setNumLegs(4);
        myPet.setName("Barnold");

        System.out.println("Your pets name is " + myPet.getName() +".");
        System.out.println("Your pet has " + myPet.getNumLegs() +" legs.");

        myPet.feed();

      //-------------------------------------------------------------------------//

        //Pet Dragon = new Pet();
        //Dragon.setNumLegs(2);
        //Dragon.setName("Smarnold");

        Dragon myDragon = new Dragon("Smarnold",4);

        System.out.println("Your dragons name is " + myDragon.getName() +".");
        System.out.println("Your dragon has " + myDragon.getNumLegs() +" legs.");

        myDragon.feed();

        //-------------------------------------------------------------------------//



    }

}
