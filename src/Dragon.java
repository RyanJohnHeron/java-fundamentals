public class Dragon {

    private String name;
    public int numLegs;


    // -----CONSTRUCTOR-----//
    public Dragon(String name, int numLegs){
        //this();
        this.setName(name);
        this.setNumLegs(numLegs);
    }


    public void feed() {
        System.out.println("Feed Dragon some yummy people.\n");
    }


    //-----GETTERS & SETTERS-----//

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }




}
